///Jeff Van Buskirk
/// CSE 002
///Professor Kalafut

import java.util.Scanner;

public class Convert {
  
  public static void main (String arg[]){ 
  
    Scanner charlie = new Scanner( System.in );
   
    
    System.out.print("How many acres of land were affected? : ");
    double acresAffected = charlie.nextDouble();
    
    System.out.print("How many inches of rain were there? : ");
    double inchesRain = charlie.nextDouble();
      
    //converting the inches to miles and acres to square miles
    inchesRain = inchesRain * 1.5782  * Math.pow (10, -5);
    acresAffected = acresAffected * 0.0015625;
    
    double cubicMiles = inchesRain * acresAffected;
    
    System.out.println(cubicMiles + " cubic miles");
    
    
    
  
  
  }
}