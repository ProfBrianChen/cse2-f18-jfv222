///Jeff Van Buskirk
/// CSE 002
///Professor Kalafut

import java.util.Scanner;

public class Pyramid {
  
  public static void main (String arg[]){ 
  
    Scanner charlie = new Scanner( System.in );
   
    
    System.out.print("What is the length of the square side? : ");
    double baseLength = charlie.nextDouble();
    
    System.out.print("What is the height of the pyramid?: ");
    double height = charlie.nextDouble();
      
    double volume = baseLength * baseLength * height / 3;
    
    System.out.println(volume);
    
    
    
  
  
  }
}