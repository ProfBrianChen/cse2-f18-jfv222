////// Jeff Van Buskirk
/// CSE 002 Hw 04
/// Professor Kalafut
//
/// porgam that generates outputs for a craps game using switches

import java.util.Scanner;

public class CrapsSwitch {
  
   public static void main (String[] args) {
     
     int dice1;
     int dice2;
     
 Scanner charlie = new Scanner( System.in );    //new scanner, I like to call mine charlie
     
     System.out.print("Would you like to randomly generate numbers or input your own? Answer Random or Mine: ");
       
       String ans1 = charlie.nextLine();
     
     switch (ans1){
         
     case "Mine":              //if the user wants to put in their own numbers than the scanner takes their inputs
           System.out.print("First number: ");
            dice1 = charlie.nextInt();
           System.out.print("Second number: ");
            dice2 = charlie.nextInt();
     break;
     
     case "Random":     //if the user wants random numbers, numbers 1 through 6 are generated independantly for each die
           dice1 = (int) (Math.random()*5+ 1);
           dice2 = (int) (Math.random()*5+ 1);
     break;
  
     default: 
           System.out.println("Unrecognized input. Try Again");   //if they dont answer Random or Mine than the program doesnt work
            dice1 = 0;
            dice2 = 0;
     break;
      
     }
     
     
     System.out.println("Your Numbers are: ");    //ouputs their dice to make sure that they got the correct input
     System.out.println(dice1 + " " + dice2);
     
     int dice3 = dice1 + dice2;   //dice3 is the total value of the two dice, i will use this value to help determine the slang response
     String slang = "";
     
     switch (dice3) {                     //finds the sslang term using a switch statement
     case 2:
       slang = "Snake Eyes";
     break;
     
     case 3:
       slang = "Ace Deuce";
     break;
     
     case 4:
       if (dice1 == dice2) {
          slang = "Hard Four";
       }
       else {
         slang = "Easy Four";
       }
     break;
     
     case 5:
        slang = "Fever Five";
     break;
     
     case 6:
       if (dice1 == dice2) {
         slang = "Hard Six";
       }
       else {
         slang = "Easy Six";
         }
     break;
     
     case 7:
       slang = "Seven Out";
     break;
     
     case 8:
       if (dice1 == dice2) {
         slang = "Hard Eight";
       }
       else {
         slang = "Easy Eight";
       }
     break;
     
       case 9:
       slang = "Nine";
     break;
     
     case 10:
       if (dice1 == dice2) {
         slang = "Hard Ten";
       }                                          //used my tree from the if statement one so the spacing is all off
       else {                                      //stil works tho
         slang = "Easy Ten";
       }
     break;
     
    case 11:
       slang = "Yo-Leven";
    break;
         
    case 12:
       slang = "Boxcars";
    break;
     
     default:
       slang = "Error try again";
     break;
     }
     System.out.println(slang + "!");
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
   }
}