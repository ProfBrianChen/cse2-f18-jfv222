////// Jeff Van Buskirk
/// CSE 002 Hw 04
/// Professor Kalafut
//
/// porgam that generates outputs for a craps game using if statments

import java.util.Scanner;

public class CrapsIf {
  
   public static void main (String[] args) {
     
     int dice1;
     int dice2;
     
 Scanner charlie = new Scanner( System.in );    //new scanner, I like to call mine charlie
     
     System.out.print("Would you like to randomly generate numbers or input your own? Answer Random or Mine: ");
       
       String ans1 = charlie.nextLine();
        
     if (ans1.equals("Mine")) {             //if the user wants to put in their own numbers than the scanner takes their inputs
     
     System.out.print("First number: ");
       dice1 = charlie.nextInt();
     System.out.print("Second number: ");
       dice2 = charlie.nextInt();
     
     }
     
     else if (ans1.equals("Random")) {    //if the user wants random numbers, numbers 1 through 6 are generated independantly for each die
      
      dice1 = (int) (Math.random()*5+ 1);
      dice2 = (int) (Math.random()*5+ 1);
       
     }
     
     else {
       System.out.println("Unrecognized input. Try Again");   //if they dont answer Random or Mine than the program doesnt work
       dice1 = 0;
       dice2 = 0;
     }
     
     System.out.println("Your Numbers are: ");    //ouputs their dice to make sure that they got the correct input
     System.out.println(dice1 + " " + dice2);
     
     int dice3 = dice1 + dice2;   //dice3 is the total value of the two dice, i will use this value to help determine the slang response
     String slang = "";
     
     if (dice3 == 2) {              //finds the slang term based off of the inputs
       slang = "Snake Eyes";
     }
     
     else if (dice3 == 3) {
       slang = "Ace Deuce";
     }
     
     else if (dice3 == 4) {
       if (dice1 == dice2) {
          slang = "Hard Four";
       }
       else {
         slang = "Easy Four";
       }
     }
     
     else if (dice3 == 5) {
        slang = "Fever Five";
     }
     
     else if (dice3 == 6) {
       if (dice1 == dice2) {
         slang = "Hard Six";
       }
       else {
         slang = "Easy Six";
         }
       }
     
    else if (dice3 == 7) {
       slang = "Seven Out";
     }
     
    else if (dice3 == 8) {
       if (dice1 == dice2) {
         slang = "Hard Eight";
       }
       else {
         slang = "Easy Eight";
       }
     }
     
    else if (dice3 == 9) {
       slang = "Nine";
     }
     
     else if (dice3 == 10) {
       if (dice1 == dice2) {
         slang = "Hard Ten";
       }
       else {
         slang = "Easy Ten";
       }
     }
     
    else if (dice3 == 11) {
       slang = "Yo-Leven";
     }
    else if (dice3 == 12) {
       slang = "Boxcars";
     }
     
     else {
       slang = "Error try again";
     }
     
     System.out.println(slang + "!");         //outputs the slang term!
          
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
   }
}