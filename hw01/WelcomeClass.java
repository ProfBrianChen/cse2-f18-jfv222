/////////////////////////
/// Jefferson Van Buskirk 9/4/2018
/// CSE 002
/// Prints a hello message to the class
/////
public class WelcomeClass{
  
  public static void main (String args []){
    /// pprints a welcome message
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("^  ^  ^  ^  ^  ^");
    System.out.println("  /\\/\\/\\/\\/\\/\\");   //used double // to avoid problem with the escape character
    System.out.println("<-J--F--V--2--2--2->");
    System.out.println("  \\/\\/\\/\\/\\/\\/");
    System.out.println("  v  v  v  v  v");
   
  }
}
