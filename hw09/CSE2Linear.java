//jeff van buskirk
//cse 2

import java.util.Scanner;
public class CSE2Linear {

    public static void main(String[] arg) {

        Scanner charlie = new Scanner(System.in);

        int[] grades = new int[15];
        System.out.println("Enter 15 grades in ascending order");

        grades[0] = getInt();
        for (int i = 1; i < grades.length; i++) {
            grades[i] = getInt();
            while (grades[i] < 0 || grades[i] > 100) {                                    //checks if within the range
                System.out.print("Please enter a score between 0 and 100: ");
                grades[i] = getInt();
                while (grades[i] < grades[i - 1]) {                                               //checks if greater than previous
                    System.out.print("Please enter an score greater than the last one: ");
                    grades[i] = getInt();
                }
            }
        }

        for (int i = 0; i < grades.length; i++) {
            System.out.print(grades[i] + " ");
        }
        System.out.println("");


        //binary searches for the
        System.out.print("Enter a score to search for: ");
        int score = getInt();
        binarySearch(score, grades);

        shuffle(grades);
        for (int i = 0; i < grades.length; i++) {
            System.out.print(grades[i] + " ");
        }
        System.out.println("");
        System.out.print("Enter a score to search for: ");
        score = getInt();
        linearSearch(score, grades);

    }

    public static int getInt() {
        Scanner charlie = new Scanner(System.in);
        while (!charlie.hasNextInt()) {
            System.out.print("Error -- please enter an integer: ");         //gets the integer and checks to make sure is an integer
            charlie.next();
        }
        return charlie.nextInt();
    }

    public static void binarySearch(int k, int[] arr) {         //binary searches
        int low = 0;
        int high = arr.length - 1;
        int mid = (low + high) / 2;
        int count = 0;

        while (low <= high) {
            count++;
            if (k == arr[mid]) {
                System.out.println(k + " was found after " + count + " iterations.");
                return;
            } else if (k < arr[mid]) {
                high = mid - 1;
                mid = (low + high) / 2;
            } else if (k > arr[mid]) {
                low = mid + 1;
                mid = (low + high) / 2;
            } else
                System.out.println("Your Code is Broken.");
        }
        System.out.println(k + "was not found after " + count + " iterations.");
    }

    public static void shuffle(int[] list) {            //shuffles the array ft. hw08
        for (int i = 0; i < 20; i++) {
            int temp = list[0];
            int pos = (int) (Math.random() * 15);
            list[0] = list[pos];
            list[pos] = temp;

        }
    }

    public static void linearSearch(int k, int[] arr){      //linear searches
        for (int i = 0; i < arr.length; i++){
            if (arr[i] == k){
                System.out.println(k + " was found after " + (i+1) + " iterations");
                return; }
        }
        System.out.println(k + " was not found after "+ arr.length + " iterations");

    }
}