//jeff van buskirk
//cse 2
import java.util.Scanner;
public class RemoveElements {
    public static void main(String[] arg) {
        Scanner scan = new Scanner(System.in);
        int num[] = new int[10];
        int newArray1[];
        int newArray2[];
        int index, target;
        String answer = "";
        do {
            System.out.println("Random input 10 ints [0-9]");
            num = randomInput();
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            newArray1 = delete(num, index);
            String out1 = "The output array is ";
            out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scan.nextInt();
            newArray2 = remove(num, target);
            String out2 = "The output array is ";
            out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer = scan.next();
        } while (answer.equals("Y") || answer.equals("y"));
    }

    public static String listArray(int num[]) {
        String out = "{";
        for (int j = 0; j < num.length; j++) {
            if (j > 0) {
                out += ", ";
            }
            out += num[j];
        }
        out += "} ";
        return out;
    }


    public static int[] randomInput() {     //random array
        int[] kelly = new int[10];
        for (int i = 0; i < kelly.length - 1; i++) {
            kelly[i] = (int) (Math.random() * 9);
        }
        return kelly;
    }

    public static int [] delete(int [] list, int pos){          //returns an array with the index specified deleted
        int [] kelly = new int [list.length -1];
        while (pos >= list.length || pos < 0){
            System.out.println("Please chose a position within the array: ");
            Scanner charlie = new Scanner (System.in);
            pos = charlie.nextInt();
        }
        int i=0;    //list index
        int k=0;    //kelly index
        while (k < kelly.length){
            if (i == pos){
                i++;
                continue;
            }
            else{
                kelly[k] = list[i];
                i++;
                k++;
            }
        }
        return kelly;
    }

    public static int [] remove(int [] list, int target){       //returns array with target value deleted
        int [] temp = new int [list.length];    //makes a temp array with same size as the passed array
        int i=0;    //list index
        int k=0;    //kelly index
        while (i < list.length){
            if (list[i] == target){
                i++;
                continue;
            }
            else{
                temp[k] = list[i];
                i++;
                k++;
            }
        }
        int [] kelly = new int [k];              //copies from the temp array
        for (int j = 0; j < kelly.length; j++){
            kelly[j] = temp[j];
        }
        return kelly;
    }
}