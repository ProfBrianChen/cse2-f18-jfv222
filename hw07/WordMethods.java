//Jefferson Van Buskirk
//CSE002 - Lab 07
//Word functions such as replacing letters and searching words


import java.util.Scanner;

public class WordMethods {

    public static void main(String arg[]){

        Scanner charlie = new Scanner (System.in);
        String thesis = "";                                                     //variables
        String kelly = "";
        thesis = getText(charlie);
        System.out.println("You Entered: " + thesis);

        do {
        
            kelly = printMenu(charlie);                        //prints the menu and does the actions until the user types q
            action(kelly, thesis, charlie);

        } while (!kelly.equals ("q"));

    }


    //other methods



    public static String getText(Scanner Input) {                   //gets the main text

        System.out.print("Enter some text, press enter when done: ");
        String text = Input.nextLine();
        return text;
    }

    public static String printMenu (Scanner Input){         //prints the menu

        System.out.println("MENU");
        System.out.println("  c - Number of non-whitespace characters ");
        System.out.println("  w - Number of words");
        System.out.println("  f - Find text");
        System.out.println("  r - Replace all !'s");
        System.out.println("  s - Shorten spaces");
        System.out.println("  q - Quit");
        System.out.print("CHOSE AN OPTION: ");

        String k = Input.next();                    //returns the decision
        Input.nextLine();
        return k;
    }

    public static void action(String choice, String sentence, Scanner input) {      //decides what method will be run on the inputted text

        switch (choice) {
            case "c" :
                System.out.println("The amount of non-whitespace characters is : " + nonWhites(sentence));
                break;
            case "w" :
                System.out.println("The amount of words is is : " + numWords(sentence));
                break;
            case "f" :
                System.out.println("Enter the text you would like to find: ");
                String word = input.next();
                System.out.println("Instances of " + word + ": " + findWord(sentence, word));
                break;
            case "r" :
                System.out.println("Edited text: " + replaceEM(sentence));
                break;
            case "s" :
                System.out.println("Edited text: " + shortenSpaces(sentence));
                break;
            case "q":
                break;
            default :
                System.out.println("Enter a valid command.");
                break;
            }
        }
    public static int nonWhites(String sentence) {         //prints the amount of non white space characters

        int count = 0;
        char k;
        for (int i = 0; i < sentence.length(); i++){
            k = sentence.charAt(i);
            if (k != ' ') {
                count++;
            }
        }
        return count;
        }

    public static int numWords(String sentence) {         //prints the amount of words

        int count = 0;
        char k;
        for (int i = 0; i < sentence.length(); i++){
            k = sentence.charAt(i);
            if (k == ' ') {
                count++;
            }
        }
        count +=1;
        return count;
    }

    public static int findWord(String sentence, String word){        //returns the amount of times a chosen word is in the text
        sentence = " " + sentence;  // had a small bug with my code because the first isntance of the word was not recognized
        int i = 0;
        int count = 0;
        if (sentence.contains(word)) {
            count = -1; //count increments an extra time because of the way the while loop increments
            while (i != -1) {
                i = sentence.indexOf(word, i + 1);
                count++;
            }
        }
        return count;
        }

    public static String replaceEM(String sentence) {        //returns string without exclamtion marks
       sentence = sentence.replace('!', '.');
        return sentence;
    }

    public static String shortenSpaces(String sentence) {           //returns string without excess spaces
        sentence = sentence.replaceAll(" +", " ");
        return sentence;
        }


    }






