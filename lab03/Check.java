///Jeff Van Buskirk
///CSE002 
///Professor Kalafut


import java.util.Scanner;
public class Check{
  public static void main(String arg[]){
    
    
    //getting inputs
    Scanner myScanner = new Scanner ( System.in );
    
    System.out.print("Enter the original Cost of the check in th form XX.xx: ");
    double checkCost = myScanner.nextDouble();
      
    System.out.print("Enter the percentage tip that you would like to add to the check as a whole numnber:");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100;      // make tip a value that is easier to multiply
    
    System.out.print("Enter the amount of people who went to dinner: ");
    int numPeople = myScanner.nextInt();
    
    
    //calculations
    double totalCost;
    double costPerPerson;
    int dollars;  //dollar amount will be used to represent the cost per person!
    int dimes;    //store the money like this in order to make decimal places easier to deal with
    int pennies;
    
    totalCost = checkCost * (1+ tipPercent);  //adds the tip to the check based of percentage requested
    costPerPerson = totalCost / numPeople;
    dollars = (int) costPerPerson;
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int)  (costPerPerson * 100) % 10;
    
    System.out.println("Each person in your group owes $" + dollars + "." + dimes + pennies);
    
    
    
    
    
  } //end of main method
} //end of class