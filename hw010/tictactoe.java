
import java.util.Scanner;
public class tictactoe {

    public static void main (String [] args) {

        String [][] board = {{"1","2","3"},{"4","5","6"},{"7","8","9"}};
        int [] used = new int [10];
        boolean won = false;
        int count = 1;
        printBoard(board);
        while (!won) {
            move(board, used, count);
            count++;
            printBoard(board);
            if (winTest(board)){
                if (count%2 == 0){
                    System.out.println("X WINS!"); }
                else {
                    System.out.println("O WINS!"); }
                won = true;
            }
            if (count == 10){
                System.out.println("DRAW");
                break;
            }

        }
    }

    public static void printBoard (String [][] board){
        for (int i = 0; i < board.length; i++){
           for (int k  = 0; k < board[i].length; k++){
               System.out.print(board[i][k] + "   ");
           }
           System.out.println();
        }
    }
    public static void move(String [][] board, int [] used, int count){
        String spot = getSpot(used);
        if (count%2 == 0){
            makeMove(spot, board, "O");
        }
        else {
            makeMove(spot, board, "X");
        }
    }
    public static String getSpot(int [] used) {
        Scanner charlie = new Scanner(System.in);
        int k = -1;
        while (k > 10 || k < 1) {
            System.out.println("Where would you like to move (integer 1-9): ");
            while (!charlie.hasNextInt()) {
                System.out.println("Where would you like to move (integer 1-9): ");
                charlie.next();
            }
            k = charlie.nextInt();
            if (chosen(k, used)) {
                System.out.println("Sorry that already taken.");
                k = -1;
            }
        }
            used[used[9]] = k;              //last item in the array is the index,
            used[9] = used[9] + 1;

            String spot = Integer.toString(k);
            return spot;

    }
    public static boolean chosen (int k, int[] arr){      //linear searches
        for (int i = 0; i < arr.length -2; i++){
            if (arr[i] == k){
                return true;
            } }
        return false;
    }
    public static void makeMove(String spot, String [][] board, String p){
        for (int i = 0; i < board.length; i++){
            for (int k  = 0; k < board[i].length; k++){
               if (spot.equals(board[i][k])){
                   board[i][k] = p;
               }
            }
        }
    }
    public static boolean winTest(String [][] board){
            boolean won = false;

            if ((board[0][0].equals(board[0][1]) && board[0][0].equals(board[0][2])) || (board[1][0].equals(board[1][1]) && board[1][0].equals(board[1][2])) || (board[2][0].equals(board[2][1]) && board[2][0].equals(board[2][2])))
                won = true;
            else if ((board[0][0].equals(board[1][0]) && board[0][0].equals(board[2][0])) || (board[0][1].equals(board[1][1]) && board[0][1].equals(board[2][1])) || (board[0][2].equals(board[1][2]) && board[0][2].equals(board[2][2])))
                won = true;
            else if ((board[0][0].equals(board[1][1]) && board[0][0].equals(board[2][2])) || (board[0][2].equals(board[1][1]) && board[0][2].equals(board[2][0])))
                won = true;


            return won;
    }
}
