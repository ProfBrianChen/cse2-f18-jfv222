///Jeff Van Buskirk
// CSE 002
/// Professor Kalafut

public class Arithmetic{
  
  public static void main (String args []){
    
    int numPants = 3;     //nunber of pants
    double pantsPrice = 34.98;   //cost per pair of pants
    
    int numShirts = 2;    //number of sweatshirts
    double shirtPrice = 24.99;  //cost per shirt
    
    int numBelts = 1;   //number of belts
    double beltPrice = 33.99;  // cost per belt
    
    double paSalestax = 0.06;   // pa tax rate
    
    double totalPants = numPants * pantsPrice; //total cost of the pants
    double totalShirts = numShirts * shirtPrice; //total cost for shirts
    double totalBelts = numBelts * beltPrice; // total cost of the belts
    
    double totalPantsTax = totalPants * paSalestax; //tax for buying all of the pants
    double totalShirtsTax = totalShirts * paSalestax; //tax for buying all shirts
    double totalBeltsTax = totalBelts * paSalestax; //tax for buying all belts
    
    double subTotal = totalPants + totalShirts + totalBelts;  //total price before tax
    double totalTax = subTotal * paSalestax;    //total tax of transaction
    
    double bigTotal = subTotal + totalTax;  //total price of transaction including tax
   
   
    ///I made a method to remove the exess decimal points based off the hints in the instructions
    totalPantsTax = twoDecimals(totalPantsTax);
    totalShirtsTax = twoDecimals(totalShirtsTax);
    totalBeltsTax = twoDecimals(totalBeltsTax);
    totalTax = twoDecimals(totalTax);
    
    bigTotal = twoDecimals(bigTotal);
 
    //outputting all of the prices
    
    System.out.println("The total price for pants is $" + totalPants);
    System.out.println("The total price for sweatshirts is $" + totalShirts);
    System.out.println("The total price for belts is $" + totalBelts);
    
    System.out.println("");   //adds a space between prices and taxes
    
    System.out.println("The total sales tax for buying pants is $" + totalPantsTax);
    System.out.println("The total sales tax for buying sweatshirts is $" + totalShirtsTax);
    System.out.println("The total sales tax for buying belts is $" + totalBeltsTax);

    System.out.println("");   //another space
    
    System.out.println("The total cost of this order before taxes is $" + subTotal);
    System.out.println("The total tax on this order is $" + totalTax);
    System.out.println("The total cost of this order after taxes is $" + bigTotal);
                
                       
    
    
    
    
  }
  
  public static double twoDecimals(double manyDecimals) {   //method to remove excess decimals
      
      double fewDecimals;                                  //I made the calculations using the instructions provided in the hints
      fewDecimals = manyDecimals * 100;                    //input is the double value and the return is that value rounded to the nearest 2 decimal value
      fewDecimals = (int) fewDecimals / 100.0;
      
      return fewDecimals;
    }
}