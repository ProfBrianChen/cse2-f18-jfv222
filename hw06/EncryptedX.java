//Jefferson Van Buskirk
//hw 06
//prints out an x using negative space


import java.util.Scanner;

public class EncryptedX {

    public static void main(String arg[]) {

        Scanner charlie = new Scanner(System.in);
        String junk = "";
        int kelly;
        do {
            System.out.print("Integer between 1 and 100: ");                  //gets int between 1 and 100

            while (!charlie.hasNextInt()) {
                junk = charlie.nextLine();
                System.out.println("Error");
                System.out.print("Integer between 1 and 100: ");
            }
            kelly = charlie.nextInt();
            junk = charlie.nextLine();

        } while (kelly < 1 || kelly > 100);


        int i;
        int j;                      //used for the for loops


        for (i = 1; i <= kelly; i++) {                  //moves the cursor to the next row
            for (j = 1; j <= kelly; j++) {
                if (j == i || j == (kelly + 1 )- i) {  //decides if it should print a space
                    System.out.print(" ");
                } else {                                //if not printing a space, it prints an X
                    System.out.print("X");
                }
            }
                System.out.println("");     //next line

        }

    }
}

