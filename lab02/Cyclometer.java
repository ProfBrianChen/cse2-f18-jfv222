/// Jeff Van Buskirk
/// CSE 002 Lab 02
/// Professor Kalafut
//
/// porgam that calculates mintues, counts, distance, for bicycle trips

public class Cyclometer {
  
   public static void main (String[] args) {
     
     //given variables
     int secsTrip1= 480; //time of trip 1
     int secsTrip2= 3220;
     int countsTrip1= 1561;  //rotations of the wheel during trip
     int countsTrip2= 9037;
     //other important variables
     double distanceTrip1;
     double distanceTrip2;
     double totalDistance;
     
     
     //conversion variables       
     double wheelDiameter = 27.0;  //diameter of bike wheelDiameter
     double PI = 3.14159;                 
     double feetPerMile = 5280;
     double inchesPerFoot = 12;
     double secondsPerMinute = 60;
     
     
     System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
     System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
     
     //calculating distance trip 1 by using the count and multiplying by the curcimfrince.
     //circumfrence is calcualated using the given value of Pi
     
     
     distanceTrip1 = countsTrip1 * wheelDiameter * PI;    //distancetrip1 is now the distance in inches
     distanceTrip1/= (inchesPerFoot * feetPerMile);         //distnacetrip1 is now the distance in miles
     
     distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot/feetPerMile; //calculating distanceTrip2 in one step. now im miles
     
     totalDistance = distanceTrip1 + distanceTrip2;   //total distance
     
    System.out.println("Trip 1 was "+distanceTrip1+" miles");       //output the results
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");
     
     
     
     
     
     
   }
}