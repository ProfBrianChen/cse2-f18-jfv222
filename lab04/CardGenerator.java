////// Jeff Van Buskirk
/// CSE 002 Lab 04
/// Professor Kalafut
//
/// porgam that generates a random card


public class CardGenerator {
  
   public static void main (String[] args) {
     
    
    int cardId;                                     
    cardId = (int) (Math.random()*51+ 1);         //creates number 1-52
     
     String suit  = "";   //suit of the card
     String cardNumber = "";    //value of the card
     
     int charlie = cardId % 13;     //remainder of this will decide the value of the card
     switch (charlie) {   
         
       case 1:
         cardNumber = "Ace";    //if the remainder is one its an ace
         break;
       case 11:
         cardNumber = "Jack";   //remainded is 11 its a jack
         break;
       case 12:
         cardNumber = "Queen";      //etc.
         break;
       case 0:
         cardNumber = "King";
         break;
       default:
         cardNumber = cardNumber + charlie;   //if the remainder is a 7 for example than the value is the 7
         break;
         
     }    
     
     int kelly = (int) cardId/4;
     
    if (kelly == 0) {
      suit = "diamonds"; }
    else if (kelly == 1) {
      suit = "clubs"; }
    else if (kelly == 2) {
      suit = "hearts"; }
    else {
      suit = "spades"; }
     
     System.out.println("You Picked the "+ cardNumber + " of "+ suit +".");
     
     
     
     
     
   }
}