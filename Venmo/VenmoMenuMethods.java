//Group K
//Jeff Van Buskirk and Archer Zhao

import java.util.Scanner;
public class VenmoMenuMethods {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int choice;
        double balance = 100;
        double money = 0;


        int SIZE = 5;
        String[] friends = new String[SIZE];
        double[] moneySent = new double[SIZE];
        double[] moneyRecieved = new double[SIZE];

        friends[0] = "Kallie";
        friends[1] = "John";
        friends[2] = "Chris";
        friends[3] = "";
        friends[4] = "";


        do {
            printMenu();
            choice = getInt();
            switch (choice) {
                case 1:
                    getBalance(balance);
                    balance = sendMoney(balance, friends, input, moneySent);
                    break;
                case 2:
                    balance = requestMoney(balance, friends, moneyRecieved);
                    break;
                case 3:
                    getBalance(balance);
                    break;
                case 4:
                    addFriend(friends, moneySent, moneyRecieved);
                    break;
                case 5:
                    printInfo(friends, moneySent, moneyRecieved);
                    break;
                case 6:
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("you entered an invalid value -- try again");
                    break;
            }
        } while (choice != 6);
    }


    //print Venmo Main Menu
    public static void printMenu() {
        System.out.println("Venmo Main Menu");
        System.out.println("1. Send Money");
        System.out.println("2. Request Money");
        System.out.println("3. Check Balance");
        System.out.println("4. Add Friend");
        System.out.println("5. Print Info");
        System.out.println("6. Quit");
    }

    //get a number and check for an integer
    public static int getInt() {
        Scanner input = new Scanner(System.in);
        while (input.hasNextInt() == false) {
            System.out.println("You entered and invalid value -- try again");
            input.nextLine();//clear buffer
        }
        return input.nextInt();
    }

    public static void getBalance(double balance) {
        System.out.println("You have $" + balance + " in your account");
    }

    //get a number and check for a double
    public static double getDouble() {
        Scanner input = new Scanner(System.in);
        while (input.hasNextDouble() == false) {
            System.out.println("You entered and invalid value -- try again");
            input.nextLine(); //clear buffer
        }
        return input.nextDouble();
    }

    //driver method for sendMoney
    public static double sendMoney(double balance, String[] friends, Scanner input, double [] sent) {
        double money = 0;
        System.out.println("How much money do you want to send?");
        money = getDouble();//get and check user input for a double
        balance = checkMoney(money, balance, friends, sent); //check valid amount no negative and if enough money is in balance to send money


        return balance;
    }

    public static int searchFriend(String[] friends, String name) {     //returns true if name is in the friends array, false if not
        int index = 0;
        while (index < friends.length) {
            if (friends[index].equals(name))
                return index;
            else
                index++;
        }
        return -1;

    }

    //method prints friend and money sent to friend
    public static void getFriend(double money, String[] friends, double [] sent) {
        Scanner input = new Scanner(System.in);
        int index = -1;
        String friend = "";
        while (index == -1) {

            System.out.println("Who do you want to send your money to?");
            friend = input.nextLine();

            if (searchFriend(friends, friend) != -1) {
                index = searchFriend(friends, friend);
            } else
                System.out.print("Error -- ");

        }
        sent[index] = sent[index] + money;
        System.out.println("You have successfully sent " + money + " to " + friend);


    }

    /*method that makes sure money entered is greater than 0,
     * there are sufficient funds in balance, if sufficient funds the balance is changed
     * and returned. if funds are not sufficient the user is taken back to the main menu
     */

    public static double checkMoney(double money, double balance, String[] friends, double [] sent) {
        while (money <= 0) {
            System.out.println("Invalid amount entered");
            money = getDouble();
        }
        if (money <= balance) {
            balance -= money;
            getFriend(money, friends, sent);
        } else {
            System.out.println("You do not have sufficient funds for this transaction.");
        }
        return balance;
    }

    //driver method for requestMonday
    public static double requestMoney(double balance, String [] friends, double [] recieved) {
        System.out.println("How much money do you want to request?");
        double money = getDouble();
        balance = checkRequestMoney(money, balance, friends, recieved);
        System.out.println("Once confirmed, your new balance will be $" + balance);
        return balance;

    }

    //gets friends information for the Request menu item and prints the amount requested from friend
    public static void getFriendRequest(double money,String [] friends, double [] recieved) {

            Scanner input = new Scanner(System.in);
            int index = -1;
            String friend = "";
            while (index == -1) {

                System.out.println("Who do you want to request money from?");
                friend = input.nextLine();

                if (searchFriend(friends, friend) != -1) {
                    index = searchFriend(friends, friend);
                } else
                    System.out.print("Error -- ");

            }
            recieved[index] = recieved[index] + money;
            System.out.println("You have successfully requested " + money + " from " + friend);


        }


    //method makes sure money is greater than 0 and adds requested amount to balance and returns new balance
    public static double checkRequestMoney(double money, double balance, String [] friends, double [] recieved) {
        while (money <= 0) {
            System.out.println("Invalid amount entered");
            money = getDouble();
        }
        balance += money;
        getFriendRequest(money, friends,recieved);
        return balance;
    }

    public static void addFriend(String[] friends, double[] sent, double[] recieved) {
        Scanner input = new Scanner(System.in);
        int index = 0;
        for (index = 0; index < friends.length; index++) {
            if (friends[index].equals("")) {
                break;
            }
        }

        System.out.print("What is your friends username? ");
        friends[index] = input.next();
        sent[index] = 0;
        recieved[index] = 0;
        }

    public static void printInfo(String [] friends, double [] sent, double [] recieved){

        System.out.println("NAME                 Money Sent              Money Recieved");
        for (int i = 0; i < friends.length; i++){
            if (friends[i] != "")
            System.out.println(friends[i] + "                $" + sent[i] + "                     $" + recieved[i]);

        }

    }
}

                
