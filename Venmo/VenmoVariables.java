////
///Venmo Variables
//Group K 
// Jeff Van BUskirk
// Chris Mann
// Archer Zhao

public class VenmoVariables{
  
  public static void main (String args []){
 
    double currentBalance = 75.00;  //current Venmo Balance
    double transactionAmt = 25.00;  //amount being sent
    double feePercent = .01;      //percentage of transaction used to generate the fee
    double transferFee = transactionAmt * feePercent ;     //total transfer fee
    double newBalance = currentBalance - transactionAmt;      //balance after transaction

    
    
    
    
    
    System.out.println("Current Balance: $" + currentBalance);
    System.out.println("Transaction Amount: $" + transactionAmt);
    System.out.println("Fee Percent: " + feePercent + "%");
    System.out.println("Transfer Fee: $" + transferFee);
    System.out.println("New Balance: $" + newBalance);
    
    
    
    
    
    
    
  }
}
