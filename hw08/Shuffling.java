import java.util.Scanner;

public class Shuffling{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //suits club, heart, spade or diamond
        String[] suitNames={"C","H","S","D"};
        String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};
        String[] cards = new String[52];

        int again = 1;
        int index = 51;
        for (int i=0; i<52; i++){
            cards[i]=rankNames[i%13]+suitNames[i/13];
            System.out.print(cards[i]+" ");
        }
        System.out.println();           //prints the array
        printArray(cards);
        System.out.println();
        shuffle(cards);                 //shuffles the array
        printArray(cards);
        System.out.println();           //prints the array again


        int numCards = 54;
        String[] hand = new String[numCards];
        while(again == 1){
            hand = getHand(cards,index,numCards);
            printArray(hand);
            index = index - numCards;
            System.out.println("Enter a 1 if you want another hand drawn");
            again = scan.nextInt();
        }
    }

    public static void printArray(String [] list){          //prints out the elements of an array with a space in between
        for (int i = 0; i < list.length; i++){
            System.out.print(list[i] + " ");
        }
    }

    public static void shuffle(String [] list){
        for (int i = 0; i < 55; i++) {
            String temp = list[0];
            int pos = (int) (Math.random() * 52);
            list[0] = list[pos];
            list[pos] = temp;
        }
    }

    public static String [] getHand(String [] list, int index, int numCards) {

        String [] hand = new String [numCards];
        for (int i = 0; i < numCards; i++) {
            if (index >= 0) {
            hand[i] = list[index]; }
            else {
                shuffle(list);  //making a new deck
                index = 52; //resetting index
                i--; }  //doesnt waste one of our cards

            index--;
        }

        return hand;

    }
}

